#  Real-time Location Tracking of IoT Devices

This project is an IoT-based platform that communicates with IoT devices using MQTT. The collected data is then processed by a message processor, such as Apache Kafka, which is responsible for converting the data into a suitable format and forwarding it to the relevant data stores and web server.

The collected data is sent to Ignition for data persistence (PostgreSQL) and to a web server to visualice each device on a 3D map in real-time.

To achieve this, two separate services have been developed:

- Visualizer
- CommunicationsManager

In order to clarify some ideas, take a look the following **IoT Backend Architecture**:

![](./docs/imgs/Project-IoT-Diagram.png)


## Summary

This IoT-based platform provides a scalable and efficient solution for collecting and visualizing data from IoT devices in real-time. It uses well-established technologies, such as MQTT and Apache Kafka, to ensure reliable data communication and processing. With the use of a 3D map, the platform offers an intuitive and interactive interface for data visualization, enabling users to quickly and easily understand the data collected from the IoT devices.

## CommunicationsManager

A Java application built using the Maven build automation tool and the Spring Boot framework. It leverages the MQTT (Message Queuing Telemetry Transport) protocol to obtain data from positioning devices and forwards it to other services using different communication protocols.

This project manages a number of communication protocols. The main use is to receive json from broker MQTT of POZYXS, get coordinates and id tag data, create a new message about it and send it or directly resend the same json.

### Build Instructions

- **Java** : JaveSE-1.7 (jdk-13.01)
- **Eclipse Java 2019**
- **Maven Integration** for Eclipse: 1.13.0.20190716-1624

### Class Structure

- **MAIN**: App.java
- **UDP Client**: clienteUDP.java
- **Robot Socket**: envioDatos class whitin App.java
- **Server Websoket**: ServerEndpoint.java
- **Logger Manager**: MyLogger.java

Each class have implemented under the class **thread**, due to this, the project is able to send the information received through one MQTT subscription to various communications protocols. Thanks to this implementation, it allows client to bring information about coordinates and tag id.

Each of the above components is responsible for a specific function:

| Service          | Class               | Purpose                                                                                                             |
|------------------|---------------------|---------------------------------------------------------------------------------------------------------------------|
| Main             | App.java            | Initialise all threads and the logger                                                                               |
| UDP Client       | clienteUDP.java     | Send data to the Ignition platform which stores it in PostgreSQL.                                                   |
| Server WebSocket | ServerEndpoint.java | Create a Websocker server to send constant data to the Visualiser.                                                  |
| Logger Manager   | MyLogger.java       | Custom Logger class to keep track of the program status and generate an HTML with the Warnning and Error level logs |



## Visualizer for POZYX

A simple HTTP server built using Python. The purpose of this server is to display a map showing the different positioning devices. The server receives data from the Java application via a WebSocket connection. WebSocket is a protocol that allows for real-time communication between a client and a server over a single, long-lived connection.

Currently, this project uses JavaScript 3D library [three.js](https://github.com/mrdoob/three.js/),it prevents time and resources from being wasted. The three.js project allows creating an easy to use, lightweight, 3D library with a default WebGL renderer.

     <script src="js/three.min.js"></script>

### Structure

Here there the principals files for to work:
```
Visualizer
│   README.md
│   server.py    
│   index.html 
│   client_websoket.js
|   style.css  
│
└───three
│   │   three.js
│   │   three.min.js
│   │   three.module.js  
│   
└───js
    │   Stats.js
    │   Detector.js
    |   ColladaLoader.js
    |   OrbitControls.js
```

Thanks to WebSocket (client_websocket.js) it is able to capture messages (JSONs) from **POZYXS** devices via the WebSocket server of '*CommunicationsManager*' (Java application). That is, '*CommunicationsManager*' captures messages from the MQTT Broker (POZYXS) and one of its tasks is to forward them via WebSocket to the "Viewer".  The use of WebSockets in the viewer allows continuous connection between client and server.

### Server HTTP
```python
# app.py

import http.server
import socketserver
import os
PORT = 8005
HOST =""

web_dir = os.path.join(os.path.dirname(__file__), './')
os.chdir(web_dir)
Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
```

### Launch

```bash
/usr/bin/python3 server.py
serving at port 8005

# http://127.0.0.1:8005/
```



## SetUP

This project has two services described above, "**Visualizer**" and "**CommunicationsManager**", in the case of the firts, it does not require any previous action for its working. But "manager" is a Maven application.

In a Maven project, creating a JAR file is a common way to package and distribute Java code. A JAR (Java ARchive) is a file format used to distribute Java classes and related resources, such as images and configuration files, in a single archive file.

### Create correctly a ejecutable Java JAR

- Create .jar project maven

     ```bash
     Run AS .. -> maven build
     # or
     Project -> Rus as -> Build.. -> Edit Configuration window    
     ```
     **Note:** *In Edit Configuration window -> Goals: clean install*

- Executable Java project named CommunicationsManager inside folder like :

          C:\Users\UserName\eclipse-workspace\CommunicationsManager

- Here there will be two JAR file, we must use **the heaviest** (usually mb of size):

          communicationsmanager-spring-boot.jar 

     **Spring-boot** *allow to create .jar file that include all dependencies*

- Run Executable in terminal:

          java -jar .\communicationsmanager-spring-boot.jar

To create **.jar** file that contains all dependencies and classes, add the following code into **pom.xml** file of project Java Maven:

```xml
<pluginManagement>
     <plugins>
          <plugin>
               <groupId>org.apache.maven.plugins</groupId>
               <artifactId>maven-compiler-plugin</artifactId>
               <version>2.3.1</version>
               <configuration>
                    <source>1.7</source>
                    <target>1.7</target>
                    <archive>
                    <manifest>
                    <addClasspath>true</addClasspath>
                    <classpathPrefix>lib/</classpathPrefix>
                    <mainClass>com.pozyx.maven.communicationsmanager.App</mainClass>
                    </manifest>
               </archive>
               </configuration>
          </plugin>
     </plugins>
</pluginManagement>
     <plugin>
     <groupId>org.apache.maven.plugins</groupId>
     <artifactId>maven-dependency-plugin</artifactId>
     <executions>
          <execution>
               <id>copy-dependencies</id>
               <phase>prepare-package</phase>
               <goals>
                    <goal>copy-dependencies</goal>
               </goals>
               <configuration>
                    <outputDirectory>${project.build.directory}/communicationsmanager/lib<outputDirectory>
                    <overWriteReleases>false</overWriteReleases>
                    <overWriteSnapshots>false</overWriteSnapshots>
                    <overWriteIfNewer>true</overWriteIfNewer>
               </configuration>
          </execution>
     </executions>
</plugin>
 <plugin>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-maven-plugin</artifactId>
    <executions>
        <execution>
            <goals>
                <goal>repackage</goal>
            </goals>
            <configuration>
                <classifier>spring-boot</classifier>
                <mainClass>
                  com.pozyx.maven.communicationsmanager.App
                </mainClass>
            </configuration>
        </execution>
    </executions>
</plugin>
</plugins>

```

## Launch

The launch is intended to be carried out under a Linux server using **systemd** to manage system settings and services. A Docker-file has also been created for each service to be able to launch with Docker and using Docker-Compose. In other words, there are two ways of implementing and launching the project and its services:

- Linux services with systemd
- Docker-Compose

### Running application as Linux service with systemd

In this case, two systemd services have been created, one for the Java application and the other for the python application. The two necessary services as well as the necessary scripts and code are contained in the "**ProjectPozyx**" folder with the following content:

```bash
.
├── CommunicationsManager.service
├── ProjectPozyx/
│   ├── CommunicationsManager/
│   │   ├── communicationsmanager.jar
│   │   ├── CommunicationsManager.sh
│   │   ├── LoggingCopy.html
│   │   └── w3.css
│   └── visualiserPOZYX/
│       ├── client_websocket.js
│       ├── three/
│       ├── index.html
│       ├── js/
│       ├── main.js
│       ├── server.py
│       ├── styles.css
│       ├── textures/
├── start.sh
└── Visualiser.service
```

**To set it up**, copy the folders "ProjectPozyx" to the path `/usr/local/bin/` or using the script named **start.sh**. This script checks, creates and copies all the directories needed to be able to launch services via systemd. Once the previous set-up is done, this script launches the services. It also allows to stop and delete them.

The following is the final result required:

Source Path:

```bash
/usr/local/bin/
│   -CommunicationsManager.sh
│
└───visualiserPOZYX/
│   │   server.py 
│   
└───CommunicationsManager/
    │   communicationsmanager.jar
    │   LoggingCopy.html
    |   w3.css
    
```

Services Path:

```sh
/etc/systemd/system/CommunicationsManager.service
/etc/systemd/system/Visualiser.service
```

Enable and start services:

```bash
sudo systemctl daemon-reload
sudo systemctl enable Service_Name
sudo systemctl start  Service_Name
sudo systemctl stop   Service_Name
```

The script **CommunicationsManager.sh** helps to manage the process generated by the launch of the generated .jar file, this script is used by **CommunicationsManager.service**:

```bash
#Init  service

./CommunicationsManager.sh start

#Reset  service

/usr/local/bin$ sudo systemctl stop CommunicationsManager
/usr/local/bin$ sudo systemctl start CommunicationsManager

```

> To restart it must be enabled before /usr/local/bin$ sudo systemctl enable CommunicationsManager

An example of how to create a service for Java and .jar files is used in the next section.

#### **Java**

Create file **.sh** under */usr/local/bin/*

Copy the following Code in **Service_Name.sh**:
Modify:

     SERVICE_NAME(Name of Service) 
     PATH_TO_JAR(Absolute Path to you jar File)
     PID_PATH_NAME(just replace Service_Name to Service_Name keeping -pid at end) 

```sh
#!/bin/sh 
SERVICE_NAME=Your_Service_Name 
PATH_TO_JAR=/usr/Name_of_User/MyJavaApplication.jar 
PID_PATH_NAME=/tmp/Service_Name-pid 
case $1 in 
start)
       echo "Starting $SERVICE_NAME ..."
  if [ ! -f $PID_PATH_NAME ]; then 
       nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >>/dev/null &      
                   echo $! > $PID_PATH_NAME  
       echo "$SERVICE_NAME started ..."         
  else 
       echo "$SERVICE_NAME is already running ..."
  fi
;;
stop)
  if [ -f $PID_PATH_NAME ]; then
         PID=$(cat $PID_PATH_NAME);
         echo "$SERVICE_NAME stoping ..." 
         kill $PID;         
         echo "$SERVICE_NAME stopped ..." 
         rm $PID_PATH_NAME       
  else          
         echo "$SERVICE_NAME is not running ..."   
  fi    
;;    
restart)  
  if [ -f $PID_PATH_NAME ]; then 
      PID=$(cat $PID_PATH_NAME);    
      echo "$SERVICE_NAME stopping ..."; 
      kill $PID;           
      echo "$SERVICE_NAME stopped ...";  
      rm $PID_PATH_NAME     
      echo "$SERVICE_NAME starting ..."  
      nohup java -jar $PATH_TO_JAR /tmp 2>> /dev/null >> /dev/null &            
      echo $! > $PID_PATH_NAME  
      echo "$SERVICE_NAME started ..."    
  else           
      echo "$SERVICE_NAME is not running ..."    
     fi     ;;
 esac
```

Give execution permission:

```sh
sudo chmod +x /usr/local/bin/Service_Name.sh
```

Test if Java App is working with:

```sh
/usr/local/bin/./Service_Name.sh start
/usr/local/bin/./Service_Name.sh stop
/usr/local/bin/./Service_Name.sh restart
```

**Create a Service**

Create a file under */etc/systemd/system/*

```bash
 /etc/systemd/system/Service_Name.service
```

Insert the following code in Service_Name:
```sh
[Unit]
 Description = Java Service
 After network.target = Service_Name.service
[Service]
 Type = forking
 Restart=always
 RestartSec=1
 SuccessExitStatus=143 
 ExecStart = /usr/local/bin/Service_Name.sh start
 ExecStop = /usr/local/bin/Service_Name.sh stop
 ExecReload = /usr/local/bin/Service_Name.sh reload
[Install]
 WantedBy=multi-user.target
```

To Test Java Application as a Service use the following commands:

```sh
sudo systemctl daemon-reload
sudo systemctl enable Service_Name
sudo systemctl start  Service_Name
sudo systemctl stop   Service_Name
```

---

#### Phyton

**Create Service**
Create a service file for the systemd as following. The file must have **.service** extension under */etc/systemd/system/* directory

```bash
[Unit]
Description=Visualiser Service
After=multi-user.target
Conflicts=getty@tty1.service

[Service]
Type=simple
ExecStart=/usr/bin/python3 /usr/local/bin/ProjectPozyx/visualiserPOZYX/server.py
StandardInput=tty-force

[Install]
WantedBy=multi-user.target
```
**Enable Newly Added Service**
```
sudo systemctl daemon-reload
```

Now enable the service to start on system boot, also start the service using the following commands.
```bash
sudo systemctl enable Visualiser.service
sudo systemctl start Visualiser.service
```

**Start/Start/Status new Service**

Use following commands to manually stop, start and restart the service:
```sh
sudo systemctl stop Visualiser.service          #To stop running service 
sudo systemctl start Visualiser.service         #To start running service 
sudo systemctl restart Visualiser.service       #To restart running service 
```
Finally check the status of service as next command:
```bash
sudo systemctl status Visualiser.service
```



## Overall help

The following command to compress an entire directory or a single file on Linux

     tar -czvf name-of-archive.tar.gz /path/to/directory-or-file

The following command will extract the contents of the archive.tar.gz file to the /tmp directory

     tar -xzvf name-of-archive.tar.gz -C /tmp
