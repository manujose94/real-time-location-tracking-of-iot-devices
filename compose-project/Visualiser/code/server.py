# app.py

import http.server
import socketserver
import os
PORT = 8005
HOST ="0.0.0.0"

web_dir = os.path.join(os.path.dirname(__file__), '.') #in sever : /usr/local/bin/
os.chdir(web_dir)
Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()