//import mqtt_client from '/client_mqtt.js'
var camera, scene, renderer;
var plane, container;
var raycaster;
var referencePointCube;
var  cube;
var objects = [];
var camera2,scene2,renderer2,axes2,cube,container2,
CANVAS_WIDTH = 200,
CANVAS_HEIGHT = 200,
CAM_DISTANCE = 300;
var zoom = 1.0, inc = -0.01;
var refreshTime_dectorTag=3000;
var mInterval1,refreshIntervalId;
//Selecting
var raycaster,mouse,INTERSECTED,projector,directionVector,statsNode,clickInfo;
var textureTAG=new THREE.MeshBasicMaterial( { color: 0x00FF80});
//Ventana
var SCREEN_HEIGHT = window.innerHeight;
var SCREEN_WIDTH = window.innerWidth;
//Rojo: x Azul: y Verde: z
//Medidas plano
var yMaxPlanta=16330;
var xMaxPlanta=7600;

var xCol=500;
var yCol=650;
var zCol=3150;
//Door
var xDoor=2650;
var yDoor=50;
var zDoor=1300;
var yBetweenWallDoor=4430;
//Table
var row_okey;
var tagsArray;
var selected_row;
window.onload=function(){
    var rows = document.getElementsByTagName("tr");
    for (var i = 0; i < rows.length; i++){
        rows[i].onclick = function() {
            if(!this) return;    
            if(selected_row)selected_row.style.backgroundColor = "white";
            selected_row= this;
            row_okey=true;
            this.style.backgroundColor = "#D5E9FF";
        }      
    }
}


init_detection();
init();
function create_mainObjects(){
    var textureCol = new THREE.TextureLoader().load( 'textures/615.jpg' );
    {
        //Plane
        //                   x               z               y
        //oxBufferGeometry(width : Float, height : Float, depth : Float
        const mesh = 
            new THREE.Mesh(new THREE.BoxBufferGeometry(xCol, zCol, yCol), 
            new THREE.MeshPhongMaterial({map: textureCol, side: THREE.DoubleSide}));
        mesh.position.set(((xMaxPlanta/2-xCol/2)), zCol/2, ((yMaxPlanta/2)-yBetweenWallDoor));
        scene.add(mesh);
      }
      {
        const mesh = 
            new THREE.Mesh(new THREE.BoxBufferGeometry(xCol, zCol, yCol), 
            new THREE.MeshPhongMaterial({map: textureCol, side: THREE.DoubleSide}));
        mesh.position.set(-((xMaxPlanta/2-xCol/2)),zCol/2,((yMaxPlanta/2)-yBetweenWallDoor) );
        scene.add(mesh);
      }
      {
        const mesh = 
            new THREE.Mesh(new THREE.BoxBufferGeometry(xCol, zCol, yCol), 
            new THREE.MeshPhongMaterial({map: textureCol, side: THREE.DoubleSide}));
        mesh.position.set(((xMaxPlanta/2-xCol/2)), zCol/2, -((yMaxPlanta/2)-yBetweenWallDoor));
        scene.add(mesh);
      }
      {
        const mesh = 
            new THREE.Mesh(new THREE.BoxBufferGeometry(xCol, zCol, yCol), 
            new THREE.MeshPhongMaterial({map: textureCol, side: THREE.DoubleSide}));
        mesh.position.set(-((xMaxPlanta/2-xCol/2)),zCol/2,-((yMaxPlanta/2)-yBetweenWallDoor) );
        scene.add(mesh);
      }
      {
        const door = 
            new THREE.Mesh(new THREE.BoxBufferGeometry(xDoor,zDoor,yDoor), 
            new THREE.MeshPhongMaterial({map: textureCol, side: THREE.DoubleSide}));
        door.position.set(-((xMaxPlanta/2-zDoor)-850),zDoor/2,-(yMaxPlanta/2) );
        scene.add(door);  
      }
    
      {
        const color = 0xFFFFFF;
        const intensity = 1;
        const light = new THREE.DirectionalLight(color, intensity);
        light.position.set(0, 10, 0);
        light.target.position.set(-5, 0, 0);
        scene.add(light);
        scene.add(light.target);
      }
}
    

function init_detection(){

   //detect
    if (!Detector.webgl) Detector.addGetWebGLMessage();
    raycaster = new THREE.Raycaster();
    mouse = new THREE.Vector2(), INTERSECTED;
    projector = new THREE.Projector();
    directionVector = new THREE.Vector3();
    //mouse info
    SCREEN_HEIGHT = window.innerHeight;
    SCREEN_WIDTH = window.innerWidth;

    clickInfo = {
            x: 0,
            y: 0,
            userHasClicked: false
        };
    //view stats
    statsNode = document.getElementById('stats');
    container = document.createElement('stats');
    
    document.body.appendChild(container);
    document.addEventListener('click', function (evt) {
        // The user has clicked; let's note this event
        // and the click's coordinates so that we can
        // react to it in the render loop
        console.log(evt)
        clickInfo.userHasClicked = true;
        clickInfo.x = evt.clientX;
        clickInfo.y = evt.clientY;
    }, false);
}  
function init() {

    camera = new THREE.PerspectiveCamera( 60, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 35000 );
    camera.position.set( -14000, 800, 3000 );
    
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xf0f0f0 );
    camera.lookAt( new THREE.Vector3(0, 0, 0) );
    
    // grid
    var gridHelper = new THREE.GridHelper( yMaxPlanta, 50 );
    scene.add( gridHelper );
  
    //texture
    // instantiate a loader
    var texture = new THREE.TextureLoader().load( 'textures/floor.jpg' );

    //Plane
    var geometry = new THREE.PlaneBufferGeometry( xMaxPlanta, yMaxPlanta );
    var material = new THREE.MeshBasicMaterial( { map: texture, side: THREE.DoubleSide} );
    geometry.rotateX(- Math.PI / 2 );
    plane = new THREE.Mesh( geometry, material  );
    plane.receiveShadow = true;
    scene.add( plane );
    objects.push( plane );
   
    
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    document.body.appendChild( renderer.domElement );


    //Cube
    var boxCube = new THREE.BoxBufferGeometry( 300, 300, 300 ); // x z y
    referencePointCube = new THREE.Mesh( boxCube, new THREE.MeshBasicMaterial( { color: 0xff0000, opacity: 1, transparent: true } ) );
    scene.add( referencePointCube );
    //loadDoor(scene);
    // cubes
   tagsArray=new Array();


    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    //controls.target.copy(cube.position);
    controls.mouseButtons = {
	    LEFT: THREE.MOUSE.RIGHT,
	    MIDDLE: THREE.MOUSE.MIDDLE,
	    RIGHT: THREE.MOUSE.LEFT
    }


    /*document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener( 'keydown', onDocumentKeyDown, false );
    document.addEventListener( 'keyup', onDocumentKeyUp, false ); **/

    window.addEventListener( 'resize', onWindowResize, false );
    document.addEventListener( 'mousemove', onMouseMove, false );
// -----------------------------------------------

    // dom
    container2 = document.getElementById('inset');

    // renderer
    renderer2 = new THREE.WebGLRenderer();
    renderer2.setClearColor( 0xf0f0f0, 4 );
    renderer2.setSize( CANVAS_WIDTH, CANVAS_HEIGHT );
    container2.appendChild( renderer2.domElement );

    // scene
    scene2 = new THREE.Scene();

    // camera
    camera2 = new THREE.PerspectiveCamera( 50, CANVAS_WIDTH / CANVAS_HEIGHT, 1, 1000 );
    camera2.up = camera.up; // important!
    
   
    // axes
    axes2 = new THREE.AxisHelper( 150 );
    console.log(axes2.geometry)
    
    scene2.add( axes2 );
        (function animate() {
            requestAnimationFrame( animate );
            controls.update();
            camera2.position.copy( camera.position );
            camera2.position.sub( controls.target );
            camera2.position.setLength( CAM_DISTANCE );
            camera2.lookAt( scene2.position );
            renderer.render(scene, camera);
            renderer2.render(scene2, camera2);
        })();
        create_mainObjects();
        render();  
        filloutTable();   
}    
function filloutTable(){
    if ($("#mytable tbody").length == 0) 
        $("#mytable").append("<tbody></tbody>");  
    $("#mytable tbody tr").remove(); 
 // Append product to the table
    for ( var i = 0; i < tagsArray.length; i ++ ) {
        $("#mytable tbody").append(
        '<tr class="w3-hover-text-green">' +
            "<td>"+tagsArray[i].userData.id+"</td>" +
            "<td>"+tagsArray[i].userData.des+"</td>" +
            "<td>"+"x:"+tagsArray[i].position.x+"  y:"+tagsArray[i].position.z+"</td>" +
            "<td>"+tagsArray[i].id+"</td>" +
        "</tr>"
        );
    }
}
function loadDoor(scene){
    if(!scene)return;
    var loader = new THREE.ObjectLoader();
    loader.load("models/door-002.json",function ( obj ) {
        scene.add( obj );
    });
}

function doSomething(x,y,z,tag) { 
    var medium=false;
    var minim=false;
    for (var i = 0; i < tagsArray.length; i++)
    {
        if(tagsArray[i].id !=tag.id){
            var value = Math.sqrt( Math.pow((tagsArray[i].position.x- tag.position.x),2)+
            Math.pow((tagsArray[i].position.z- tag.position.z),2));
            if(value<1400) minim=true;
            else if(value<3400) medium=true;

           
            //if(Math.abs(tagsArray[i].position.x- tag.position.x)<700) minim=true;
            //if(Math.abs(tagsArray[i].position.z- tag.position.z)<700) minim=true;
        }
    }

    if(medium) tag.material.color.setHex( 0xA6A948 );
    else if(minim) tag.material.color.setHex( 0xD6614C );
    else tag.material.color.setHex( 0x3b5998 ); 
        // Filtro de media 
       /** c/2 + x/2; **/
   /*tag.position.x = tag.position.x/2 + x/2; 
    tag.position.z = tag.position.z/2 + y/2; **/
    tag.position.x = x;
    tag.position.z = y;
    //Zona muerta
   // var x2 =tag.position.x;
    //var y2 =tag.position.z;
    //  tag.position.x X'
    //  tag.position.z Y'
   // if(Math.abs(x2-x)>80)
      //  tag.position.x = x;

   // if(Math.abs(y2-y)>80)
     //   tag.position.z = y;
    //console.log(" Y:"+y+" X:"+x);
    //tag.position.y = 0;
 
}

// Intervalo de tiempo, detectar posibles caidas de tags
refreshIntervalId = setInterval(function(){ 
    for (var i = 0; i < tagsArray.length; i++)
    {
        var startTime = tagsArray[i].userData.time
        var endTime = new Date();
        var timeDiff = endTime - startTime; //in ms
        // strip the ms
        timeDiff /= 1000;       
        // get seconds 
        var seconds = Math.round(timeDiff);
        //console.log(tagsArray[i].userData.id+" -> "+seconds+" - "+refreshTime_dectorTag)
       if(seconds>=3){
        //console.log("Tag: "+tagsArray[i].userData.id+" died");
        scene.remove(tagsArray[i]);
        tagsArray.splice(i, 1);
        //console.log(tagsArray);
        filloutTable();
       }else 
       tagsArray[i].userData.time= new Date();
    }
     }, refreshTime_dectorTag);


function tagsRefresh(id,coordinates){
var existTag = tagsArray.find( tag => tag['userData'].id === id );
//Eliminar
//console.log(id+" Total tags: "+tagsArray.length)
//Añadir
if(!existTag){ //Create new Tag+
    if(!coordinates) return;
    cube = new THREE.Mesh( new THREE.BoxBufferGeometry( 200, 250, 200 ), new THREE.MeshBasicMaterial( { color: 0x3b5998}) );
    cube.name=id;
    cube.userData = { id: id , des: "Tag of POZYX", time: new Date(), medium: false };
    cube.position.x = coordinates.x;
    cube.position.y = 0;
    cube.position.z = coordinates.y;
    console.log("new tag: "+id)
    cube.updateMatrix();
    scene.add(cube);
    //añade al final de un array
    tagsArray.push(cube);
    //console.log("New Tag refresh information")
    //console.log(tagsTimeLifeArray)
    filloutTable();
    //if(coordinates)
    //doSomething(coordinates.x,coordinates.y,0,tagsArray[tagsArray.length-1]);
}else{
    //Actualizar
    existTag.userData.time= new Date();
    if(coordinates) //Move Tag
    doSomething(coordinates.x,coordinates.y,0,existTag);
}

}

socket.onmessage = function(event) {
    var message = JSON.parse(event.data);
    //console.log("message",message)
    var coordinates;
    if(!message) return;
    if(message.data)if(message.data.coordinates) coordinates=message.data.coordinates;
    console.log(coordinates);
    //Detect if exist TAG and Move it
    tagsRefresh(message.tagId,coordinates);
};

  
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
function onDocumentMouseMove( event ) {
    event.preventDefault();
    mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( objects );
    if ( intersects.length > 0 ) {
        var intersect = intersects[ 0 ];
        rollOverMesh.position.copy( intersect.point ).add( intersect.face.normal );
        rollOverMesh.position.divideScalar( 50 ).floor().multiplyScalar( 50 ).addScalar( 25 );
    }
    render();
}


function onMouseMove( event ) {
    event.preventDefault();
    // calculate mouse position in normalized device coordinates
    // (-1 to +1) for both components

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

   

}

function render() {
    var id=null;
    if (clickInfo.userHasClicked) {

        clickInfo.userHasClicked = false;

        statsNode.innerHTML = '';

        // The following will translate the mouse coordinates into a number
        // ranging from -1 to 1, where
        //      x == -1 && y == -1 means top-left, and
        //      x ==  1 && y ==  1 means bottom right
        var x = ( clickInfo.x / SCREEN_WIDTH ) * 2 - 1;
        var y = -( clickInfo.y / SCREEN_HEIGHT ) * 2 + 1;

        // Now we set our direction vector to those initial values
        directionVector.set(x, y, 1);

        // Unproject the vector
        projector.unproject(directionVector, camera);

        // Substract the vector representing the camera position
        directionVector.sub(camera.position);

        // Normalize the vector, to avoid large numbers from the
        // projection and substraction
        directionVector.normalize();

        // Now our direction vector holds the right numbers!
       // update the picking ray with the camera and mouse position
        raycaster.setFromCamera( mouse, camera );

        var intersects = raycaster.intersectObjects(tagsArray); //array
        console.log(intersects)
        //var intersects = ray.intersectObjects(scene.children);
        if (intersects.length) {
            if ( INTERSECTED != intersects[ 0 ].object ) {
                //Solo los objetos que sean tags
                if(intersects[ 0 ].object) 
                if ( INTERSECTED ) INTERSECTED.material.color.setHex( 0x00FF80 );
                INTERSECTED = intersects[ 0 ].object;
                console.log(INTERSECTED.name)
               
                INTERSECTED.material.color.setHex( 0xd5e9ff );
        
        }
            // intersections are, by default, ordered by distance,
            // so we only care for the first one. The intersection
            // object holds the intersection point, the face that's
            // been "hit" by the ray, and the object to which that
            // face belongs. We only care for the object itself.
            var target = intersects[0].object;
            id=target.id;
            statsNode.innerHTML = 'Name: ' + target.name
                    + '<br>'
                    + 'ID: ' + target.id;

    
        }
        if(row_okey)setSelectedObject(selected_row.cells);
            else{
               if(id) findObjectTable(id);
                renderer.render( scene, camera );
            }

    }

    function findObjectTable(id){ 
        var rows = document.getElementsByTagName("tr");
        for (var i = 0; i < rows.length; i++){
            if(rows[i].cells)if(rows[i].cells.length>1){
                if(rows[i].cells[3].innerHTML==id){
                    //console.log(rows[i].cells[0].innerHTML+' '+id);
                    if(selected_row)selected_row.style.backgroundColor = "white";
                    selected_row= rows[i];
                    selected_row.style.backgroundColor = "#D5E9FF";
                    i=rows.length;
                }
            }       
        }
    }

    //Seleccion de objeto desde la tabla
    function setSelectedObject(row) {
        row_okey=null;
        var id = row[3].innerHTML;
        if(!id) return;
        var data = tagsArray.find( tag => tag['id'] === Number(id) );
        if( data ) {    
            document.getElementById("stats").innerHTML  = '';
            //console.log( 'found' );
            if ( INTERSECTED != data ) {
                        if ( INTERSECTED ) INTERSECTED.material.color.setHex( 0x00FF80 );
                        INTERSECTED = data;               
                        INTERSECTED.material.color.setHex( 0xd5e9ff );     
                }   
                var target = data;
                document.getElementById("stats").innerHTML  = 'Name: ' + target.name
                        + '<br>'
                        + 'ID: ' + target.id;
                        renderer.render( scene, camera );
        }
    }
    requestAnimationFrame(render);
    renderer.render( scene, camera );
    renderer2.render( scene2, camera2 );
}
