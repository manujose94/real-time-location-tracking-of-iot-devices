const mqtt = require('mqtt');
const data = require('./data.json');

// MQTT broker configuration
const brokerUrl = 'mqtt://localhost';
const brokerPort = 1883;
const client = mqtt.connect(`${brokerUrl}:${brokerPort}`);

// Send data periodically
const intervalMs = 1000; // Send data every 5 seconds
setInterval(() => {
  const message = JSON.stringify(data);
  client.publish('tags', message);
  console.log(`Data sent: ${message}`);
}, intervalMs);

// Handle disconnection
client.on('close', () => {
  console.log('Disconnected from MQTT broker');
});