package com.pozyx.maven.communicationsmanager.server;
import static java.lang.String.format;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import com.pozyx.maven.communicationsmanager.App;
@javax.websocket.server.ServerEndpoint(value = "/chat",
decoders = MessageDecoder.class, 
encoders = MessageEncoder.class 
		)
public class ServerEndpoint implements MqttListener {

    static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    public static Thread hilo;
   public static ClientWebSocketRunnable myRunnable;
    @OnOpen
    public void onOpen(Session session) {
        //System.out.println(format("%s joined the chat room.", session.getId()));
        peers.add(session);
        final Session misesion = session;
        myRunnable = new ClientWebSocketRunnable(misesion);
        System.out.println("[WebSocket] New Client "+myRunnable+ " with session ID:"+session.getId() );
        App.LOGGER.info("[WebSocket] New Client "+myRunnable+ " with session ID:"+session.getId() );
        hilo = new Thread( myRunnable);
        hilo.start();   
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException, EncodeException {
    	//Arrived message from any client
        //broadcast the message
        //for (Session peer : peers) {
        	//System.out.println(peer.getId());
        	// session.getBasicRemote().sendObject("viva HALO !! ");
        //}
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        System.out.println(format("%s left the chat room.", session.getId()));
        App.LOGGER.info("[WebSocket] Client with "+session.getId()+" session has been closed");
        //Must stop the current thread
        if(myRunnable!=null)myRunnable.stop();
        peers.remove(session);
      
    }

	public void onMessageArrived(String message) {
		// TODO Auto-generated method stub
		System.out.println("Server receive: "+message);
		 for (Session peer : peers) {
	        	System.out.println(peer.getId());
	        	try {
					peer.getBasicRemote().sendObject("Message recived in Sever WebSocket");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (EncodeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
	}

}