package com.pozyx.maven.communicationsmanager;
import  java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.io.IOException;
import java.net.DatagramPacket;
public class EchoServer extends Thread {
	 
    private DatagramSocket socket;
    private boolean running;
    private byte[] buf = new byte[1024];
    byte[] receive = new byte[65535]; 
    public EchoServer() {
        try {
        	System.out.println("Starting UDP Sever......");
			socket = new DatagramSocket(6005);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
 
    public void run() {
    	
        running = true;
 
        while (running) {
            DatagramPacket packet 
              = new DatagramPacket(buf, buf.length);
            try {
            	System.out.println("[Server] Esperando a recibir paquete");
				socket.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            String data_received = new String(packet.getData());
            data_received = "[SERVER] "+data_received;
            
            InetAddress address = packet.getAddress();
            int port = packet.getPort();
            packet = new DatagramPacket(data_received.getBytes(), data_received.getBytes().length, address, port);
            String received 
              = new String(packet.getData(), 0, packet.getLength());
            received = "[SERVER] "+received;
            if (received.equals("end")) {
                running = false;
                continue;
            }
            try {
				socket.send(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        socket.close();
    }
}