package com.pozyx.maven.communicationsmanager.server;

public interface MqttListener {
	 void onMessageArrived(String message);
}
