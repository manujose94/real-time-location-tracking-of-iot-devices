package com.pozyx.maven.communicationsmanager;



import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

// this custom formatter formats parts of a log record to a single line
class MyHtmlFormatter extends Formatter {
    // this method is called for every log records
    public String format(LogRecord rec) {
        StringBuffer buf = new StringBuffer(1000);
        buf.append("<tr>\n");

        // colorize any levels >= WARNING in red
        if (rec.getLevel().intValue() >= Level.WARNING.intValue()) {
            buf.append("\t<td style=\"color:red\">");
            buf.append("<b>");
            buf.append(rec.getLevel());
            buf.append("</b>");
        } else {
            buf.append("\t<td>");
            buf.append(rec.getLevel());
        }

        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(calcDate(rec.getMillis()));
        buf.append("</td>\n");
        buf.append("\t<td>");
        buf.append(formatMessage(rec));
        buf.append("</td>\n");
        buf.append("</tr>\n");

        return buf.toString();
    }

    private String calcDate(long millisecs) {
        SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
        Date resultdate = new Date(millisecs);
        return date_format.format(resultdate);
    }

    // this method is called just after the handler using this
    // formatter is created
    public String getHead(Handler h) {
        return "<!DOCTYPE html>\n<head>\n<style>\n"
            + "table { width: 100% }\n"
            + "th { font:bold 10pt Tahoma; }\n"
            + "td { font:normal 10pt Tahoma; }\n"
            + "h1 {font:normal 11pt Tahoma;}\n"
            + "</style>\n"
            + "<link type=\"text/css\" rel=\"stylesheet\" href=\"w3.css\">"
            + "</head>\n"
            + "<body>\n"
            +"<input class=\"w3-input w3-border w3-padding\" type=\"text\" placeholder=\"Search for names..\" id=\"myInput\" onkeyup=\"myFunction()\">"
            + "<h1>" + (new Date()) + "</h1>\n"
            + "<table border=\"0\" class=\"w3-table-all w3-margin-top\" cellpadding=\"5\" cellspacing=\"3\" id=\"myTable\">\n"
            + "<tr align=\"left\">\n"
            + "\t<th style=\"width:10%\">Loglevel</th>\n"
            + "\t<th style=\"width:15%\">Time</th>\n"
            + "\t<th style=\"width:75%\">Log Message</th>\n"
            + "</tr>\n"
        	+ "<script>\r\n" + 
        	"function myFunction() {\r\n" + 
        	"  var input, filter, table, tr, td, i;\r\n" + 
        	"  input = document.getElementById(\"myInput\");\r\n" + 
        	"  filter = input.value.toUpperCase();\r\n" + 
        	"  table = document.getElementById(\"myTable\");\r\n" + 
        	"  tr = table.getElementsByTagName(\"tr\");\r\n" + 
        	"  for (i = 0; i < tr.length; i++) {\r\n" + 
        	"    td = tr[i].getElementsByTagName(\"td\")[0];\r\n" + 
        	"    if (td) {\r\n" + 
        	"      txtValue = td.textContent || td.innerText;\r\n" + 
        	"      if (txtValue.toUpperCase().indexOf(filter) > -1) {\r\n" + 
        	"        tr[i].style.display = \"\";\r\n" + 
        	"      } else {\r\n" + 
        	"        tr[i].style.display = \"none\";\r\n" + 
        	"      }\r\n" + 
        	"    }\r\n" + 
        	"  }\r\n" + 
        	"}\r\n" + 
        	"</script>\r\n";
        	
      }

    // this method is called just after the handler using this
    // formatter is closed
    public String getTail(Handler h) {
        return "</table>\n</body>\n</html>";
    }
}
