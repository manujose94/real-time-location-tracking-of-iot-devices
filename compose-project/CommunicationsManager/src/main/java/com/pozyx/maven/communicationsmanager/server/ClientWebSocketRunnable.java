package com.pozyx.maven.communicationsmanager.server;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.websocket.EncodeException;
import javax.websocket.Session;

import com.pozyx.maven.communicationsmanager.App;

public class ClientWebSocketRunnable implements Runnable {
	

		String json;
	
		boolean new_message=false;
		Session msession = null;
		   AtomicBoolean running = new AtomicBoolean(false);
		     AtomicBoolean stopped = new AtomicBoolean(true);
		
		  public ClientWebSocketRunnable(Session msessio){
		        this.msession = msessio;
		    }
		  
		  public void stop() {
		        running.set(false);
		    }

		    public void interrupt() {
		        running.set(false);
		      
		    }

		    boolean isRunning() {
		        return running.get();
		    }

		    boolean isStopped() {
		        return stopped.get();
		    }
		  
		
		public void setMessage(String json) {
			this.json=json;
			//System.out.println("[Cliente Websocket] set");
			new_message=true;
		}
		
		public void run() {
			// TODO Auto-generated method stub
			 running.set(true);
		        stopped.set(false);
				try {
					App.LOGGER.info("[Websocket Client] Thread Runnable started (MyRunnable)");
					System.out.println("[Websocket Client] Thread Runnable started");
					while(running.get()) {
						if(new_message) { // To improve must use array
							//System.out.println("[Websocket Client] sending new data");
							msession.getBasicRemote().sendObject(this.json);
							//System.out.println("[Websocket Client] sended");
							new_message=false;
						 }
						try {
			                Thread.sleep(100);
			            } catch (InterruptedException e) {
			            	Thread.currentThread().interrupt();
			                System.out.println("Thread was interrupted, Failed to complete operation");
			                App.LOGGER.severe("[Websocket Client] Thread was interrupted, Failed to complete operation");
			            }
					} 
					 stopped.set(true);
					 App.LOGGER.info("[Websocket Client] the connection has been closed");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					 App.LOGGER.severe("[Websocket Client] the connection has been closed: "+e.getMessage());
					//e.printStackTrace();
				} catch (EncodeException e) {
					// TODO Auto-generated catch block
					 App.LOGGER.severe("[Websocket Client] the connection has been closed: "+e.getMessage());
					//e.printStackTrace();
				} catch (IllegalStateException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
					App.LOGGER.severe("[Websocket Client] the connection has been closed: "+e.getMessage());
					System.out.println("[Websocket Client] the connection has been closed");
				}
		}
	
	
}
