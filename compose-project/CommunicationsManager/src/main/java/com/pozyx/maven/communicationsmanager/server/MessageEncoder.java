package com.pozyx.maven.communicationsmanager.server;


import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;

public class MessageEncoder implements Encoder.Text<Message> {
	private static Gson gson = new Gson();
 
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}

	public String encode(Message message) throws EncodeException {
		// TODO Auto-generated method stub
		return gson.toJson(message);
	}

}
