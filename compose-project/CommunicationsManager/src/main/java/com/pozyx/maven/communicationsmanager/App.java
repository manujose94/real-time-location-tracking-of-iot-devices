package com.pozyx.maven.communicationsmanager;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.websocket.DeploymentException;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.pozyx.maven.communicationsmanager.server.ServerEndpoint;

import java.util.logging.Level;
import java.util.logging.Logger;
public class App
{

    final static String serverUrl = System.getenv("MQTT_SERVER_URL");     /* ssl://mqtt.cumulocity.com:8883 for a secure connection */
	private static final int MAX_RETRIES = 10;
    private static final int RETRY_INTERVAL_MS = 5000;
    final static String clientId    = "my_mqtt_java_client";
    final static String device_name = "My Java MQTT device";
    final static String tenant      = "<<tenant>>";
    final static String username    = "<<username>>";
    final static String password    = "<<password>>";
    final static String topic        = "tags";
    final static String json_string= "{\"version\":\"1.4\",\"timestamp\":1571739793.0329084,\"success\":true,\"data\":{\"extras\":{\"version\":\"0.1\",\"zones\":[]},\"anchorData\":[{\"anchorId\":\"48273\",\"rss\":-84.52,\"tagId\":\"27191\"},{\"anchorId\":\"25779\",\"rss\":-83.63,\"tagId\":\"27191\"},{\"anchorId\":\"2960\",\"rss\":-84.94,\"tagId\":\"27191\"},{\"anchorId\":\"39481\",\"rss\":-86.59,\"tagId\":\"27191\"},{\"anchorId\":\"33684\",\"rss\":-84.34,\"tagId\":\"27191\"},{\"anchorId\":\"14731\",\"rss\":-89.55,\"tagId\":\"27191\"}],\"coordinatesType\":1,\"metrics\":{\"latency\":31,\"rates\":{\"update\":10.33,\"success\":9.54,\"packetLoss\":0.14}},\"tagData\":{\"linearAcceleration\":{\"z\":-29,\"x\":5,\"y\":-3},\"customSensors\":[{\"value\":{\"type\":\"Buffer\",\"data\":[]},\"name\":\"customPayload\"}],\"magnetic\":{\"z\":-40.875,\"x\":27.5625,\"y\":8.875},\"gravityVector\":{\"z\":980,\"x\":9,\"y\":-29},\"gyro\":{\"z\":0.125,\"x\":0.0625,\"y\":0.0625},\"acceleration\":{\"z\":950,\"x\":15,\"y\":-33},\"pressure\":100987.25,\"maxLinearAcceleration\":2.784,\"blinkIndex\":10652,\"quaternion\":{\"z\":-0.01318359375,\"x\":0.80291748046875,\"w\":0.5958251953125,\"y\":-0.00921630859375},\"eulerAngles\":{\"z\":1.6875,\"x\":286.8125,\"y\":0.5625}},\"coordinates\":{\"z\":1000,\"y\":6200,\"x\":27827}},\"tagId\":\"27191\"}";       
    static String data;
    //WebSocket
    static String dataWebsocket=null;
   	static String serverWebSocket = System.getenv("WEBSOCKET_SERVER");
	static int portWebSocket = Integer.parseInt(System.getenv("WEBSOCKET_PORT"));
    
    // Direcciones IP
    // Server Address: 158.42.163.97
    // Laptop Address: 158.42.57.151
    
    //Cliente UDP ->  Connection to UDP SERVER in Ignition
    static String serverUDP = System.getenv("UDP_SERVER");
	static int portUDP = Integer.parseInt(System.getenv("UDP_PORT"));
	//Port Robot Connection
	static int portSocket = Integer.parseInt(System.getenv("SOCKET_PORT"));
    
    private static ServerSocket serverSocket = null;
    private static Socket clientSocket = null;
    private static PrintWriter os = null;
    private static InputStream is = null;
    static byte[] buffer= new byte[1024];
	private static int datosRecibidos;
	
   //LOGGER
	public final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
	private static String levelLog="2";
	private static String pathLog=null;
    public static void main( String[] args )
    {     
    	if(args.length>0)levelLog=args[0];
    	if(args.length>1) {
    		pathLog=args[1];
    		File f = new File(pathLog);
    		if(f.exists() && f.isDirectory()) {
    			pathLog=args[1];
    			System.out.println("Level log:"+levelLog+":=>"+pathLog);
    		}else pathLog="";
    	}
    	
    	if(levelLog=="2")LOGGER.setLevel(Level.SEVERE);
    	else if(levelLog=="1")LOGGER.setLevel(Level.WARNING);
    	else if(levelLog=="0")LOGGER.setLevel(Level.INFO);
    	//1. Instance Logger
    	 try {
             MyLogger.setup(pathLog);
         } catch (IOException e) {
             e.printStackTrace();
             throw new RuntimeException("Problems with creating the log files");
         }

		 if(serverUrl != null) {
    		System.out.println("The value of MQTT_SERVER_URL is: " + serverUrl);
		} else {
			System.out.println("MQTT_SERVER_URL is not set.");
		}
    	//1. Initial WebSocket Server
    	//Laptop Address
        org.glassfish.tyrus.server.Server server = new org.glassfish.tyrus.server.Server(serverWebSocket, portWebSocket, "/ws", ServerEndpoint.class);
       
	        try {
	            server.start();
	            LOGGER.info("[WebSocket] started with: "+serverWebSocket+":"+portWebSocket);
	        } catch (DeploymentException e) {
	            throw new RuntimeException(e);
	        } finally {
	            //server.stop();
	        	LOGGER.info("[WebSocket] started with: "+serverWebSocket+":"+portWebSocket);
	        	System.out.println("Sever Websocket Iniciado!!");
	        }
	        
	   /**     
	    * Timer Test
	    * 
	    * TimerTask timerTask = new TimerTask() {
	            @Override
	            public void run() {
	                //System.out.println( ServerEndpoint.myRunnable);
	                if( ServerEndpoint.myRunnable!=null)
	                	ServerEndpoint.myRunnable.setMessage(json_string);
	            }
	        };
	        Timer timer = new Timer(true);
	        timer.scheduleAtFixedRate(timerTask, 0, 3 * 1000); **/
	        
	    //2. Initial UDP Cliente to Ignition 158.42.163.97
    	final Data msg = new Data("Firts:0:0:0;");
    	
    	//Info:			        new clienteUDP(hostname,port,Data,timer)
    	clienteUDP clientedup = new clienteUDP(serverUDP,portUDP,msg,100);
    	
        
    	new Thread(clientedup,"clientedup").start();
    	LOGGER.info("[clienteUDP] started with: "+serverUDP+":"+portUDP);
    	
    	
    	
    	//4. Create ServerSocket to a Robot (TCP)
    	envioDatos.ServerSocketRobot();
    	
    	//3. Initial MQTT Client to Broker MQTT POZYX 

        // MQTT connection options
        final MqttConnectOptions options = new MqttConnectOptions(); 
		options.setAutomaticReconnect(true);  
		options.setCleanSession(true);
		int retries = 0;
        boolean connected = false;
		while (!connected && retries < MAX_RETRIES) {
            try {
				final MqttClient client = new MqttClient(serverUrl, clientId, null);
				
				System.out.println("[MqttClient] connecting to broker: "+serverUrl);
				LOGGER.info("[MqttClient] connecting to broker: "+serverUrl);
				
				client.connect(options);
				
				System.out.println("[MqttClient] connected to "+serverUrl);
				LOGGER.info("[MqttClient] connected to "+serverUrl);
				connected = true;
			
				client.subscribe(topic, new IMqttMessageListener() {
					public void messageArrived (final String topic, final MqttMessage message) throws Exception {
						String payload = new String(message.getPayload());
						if(payload!=null) payload= payload.substring(1,payload.length()-1);     
		//              if(!App.alreadyExecuted) { //write only once
		//                	App.alreadyExecuted=true;
		//                	try (FileWriter file = new FileWriter("output.txt")) {
		//            			file.write(payload);
		//            		}
		//                }
					
					//JsonParseException - if the specified text is not valid JSONJsonSyntaxException
						
				//Send to Visualizador WebSocket
				if(ServerEndpoint.myRunnable!=null) {ServerEndpoint.myRunnable.setMessage(payload);}
				JsonObject jsonObject=null;
					try {
					jsonObject = new JsonParser().parse(payload).getAsJsonObject();
					}catch(JsonParseException jsonEx) {
						LOGGER.severe("[MqttClient] Warning Log: [JsonParseException]:: "+jsonEx.getMessage());jsonObject=null;
					}catch(IllegalStateException illegal) {
						LOGGER.severe("[MqttClient] Warning Log: [IllegalStateException]:: "+illegal.getMessage());jsonObject=null;
					}
					
					if(jsonObject!=null) {
						
							if(jsonObject.get("tagId")!=null){
								//System.out.println(data);
								//ID:x:y:z;
								data=jsonObject.get("tagId").getAsString();
								if(jsonObject.getAsJsonObject("data")!=null && jsonObject.getAsJsonObject("data").getAsJsonObject("coordinates") !=null ){
									String x = null,y = null,z = null;
									x=jsonObject.getAsJsonObject("data").getAsJsonObject("coordinates").get("x").getAsString();
									y=jsonObject.getAsJsonObject("data").getAsJsonObject("coordinates").get("y").getAsString();
									z=jsonObject.getAsJsonObject("data").getAsJsonObject("coordinates").get("z").getAsString();		                	
									data=data+":"+x;
									data=data+":"+y;
									data=data+":"+z;
									data=data+";";
									//System.out.println(data);
								}else data=null;
							}
							if(data==null) LOGGER.warning("[MqttClient] Warning Log: "+"bad data");
							else LOGGER.info("[MqttClient] Info Log: "+data);

							// SEND MESSAGE TO SERVER UPD WITH NOTIFY NEW DATA IN MSG VARIABLE             
							synchronized (msg) {
								if(data!=null) {
								msg.setMsg(data);
								msg.notify();
								}// msg.notifyAll();
							}
							if (payload.startsWith("510")) {
								// execute the operation in another thread to allow the MQTT client to
								// finish processing this message and acknowledge receipt to the server
								Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
									public void run() {
										try {
											System.out.println("[MqttClient] Simulating device restart...");
											LOGGER.severe("[MqttClient] Simulating device restart...");
											client.publish("s/us", "501,c8y_Restart".getBytes(), 2, false);
											System.out.println("...restarting...");
											Thread.sleep(TimeUnit.SECONDS.toMillis(5));
											client.publish("s/us", "503,c8y_Restart".getBytes(), 2, false);
											System.out.println("...done...");
										} catch (MqttException e) {
											e.printStackTrace();
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								});
							}
						}
					}
				});
        } catch(MqttException me) { // Catch error
        	  System.out.println("reason "+me.getReasonCode());
              System.out.println("msg "+me.getMessage());
              System.out.println("loc "+me.getLocalizedMessage());
              System.out.println("cause "+me.getCause());
              System.out.println("excep "+me);
              retries++;
              System.err.println("[MqttClient] failed to connect to broker: " + serverUrl + ", retrying in " + RETRY_INTERVAL_MS + "ms (attempt " + retries + " of " + MAX_RETRIES + ")");
                try {
                    Thread.sleep(RETRY_INTERVAL_MS);
                } catch (InterruptedException ex) {}
        }
	}
    }
    
 
public  static class envioDatos extends Thread {
	
	   public static void  ServerSocketRobot() {
	    	envioDatos hiloEnvio;
	    	hiloEnvio=new envioDatos();
	    	hiloEnvio.start();
	    }
	    
    	
    	public envioDatos() {
    		try {
                serverSocket = new ServerSocket(portSocket); //Crear el servidor indicandole el puerto de escucha:   
           } catch (IOException e) {
                System.exit(1);
           }
    	}
    	
    	public void run() {
    		while(true) {
	    		try {
	    			System.out.println("[ServerSocketRobot] Wait to new client connected in "+"158.42.163.97:"+portSocket);
	    			LOGGER.info("[ServerSocketRobot] Wait to new client connected");
	    			
	                clientSocket = serverSocket.accept();//Esperar la conexion de un cliente y obtener el socket que identifica la conexion
	                
	            	LOGGER.info("[ServerSocketRobot] New client connected");
	                System.out.println("[ServerSocketRobot] New client connected");
	                is = clientSocket.getInputStream();
	                os = new PrintWriter(clientSocket.getOutputStream());//Vincular el stream de salida del socket con un PrintWriter
	               } catch (IOException e) {
	                  System.err.println("[ServerSocketRobot]Error enviando/recibiendo: " + e.getMessage());
	                  LOGGER.severe("[ServerSocketRobot]Error enviando/recibiendo: " + e.getMessage());
	                 // e.printStackTrace();
	               }
	    		 do {
	    				os.println(data);
	    				LOGGER.info("[ServerSocketRobot] data: "+data);
	    				os.flush();
	    				try {			
		    					datosRecibidos = is.read(buffer);
		    			
	    				} catch (IOException e) {
	    					// TODO Auto-generated catch block
	    					System.out.println("[ServerSocketRobot]Error in datosRecibidos = is.read(buffer)");
	    					 LOGGER.severe("[ServerSocketRobot]Error in datosRecibidos = is.read(buffer)");
	    				}
	    			//	String output = new String(buffer, 0, datosRecibidos);  
	    		}while(datosRecibidos>-1);
	    		try {
					is.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("[ServerSocketRobot] Possible error closing the connection with the client");
					LOGGER.severe("[ServerSocketRobot] Possible error closing the connection with the client");
				}
				os.close();
    		}
    	}
	}
}