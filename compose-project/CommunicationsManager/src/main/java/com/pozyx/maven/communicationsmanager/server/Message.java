package com.pozyx.maven.communicationsmanager.server;

import java.util.Date;

public class Message {

	   private String content;
	    private String to;
	    private Date from;
	    // getters and setters
		public String getContent() {
			return content;
		}
		public void setContent(String content) {
			this.content = content;
		}
		public String getTo() {
			return to;
		}
		public void setTo(String to) {
			this.to = to;
		}
		public Date getFrom() {
			return from;
		}
		public void setFrom(Date from) {
			this.from = from;
		}
	    
	    
	}