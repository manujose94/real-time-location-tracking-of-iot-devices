package com.pozyx.maven.communicationsmanager;

import java.io.*;
import java.net.*;
 
/**
 * This program demonstrates how to implement a UDP client program.
 *
 *
 * @author www.codejava.net
 */
public class clienteUDP  implements Runnable {
	private InetAddress address;
	private DatagramSocket socket;
	  private boolean running=false;
	 String hostname = "";
	int port=0;
	 private Data msg;
	 private int timer;
	
	   public clienteUDP(String hostname, int port, Data m, int timer) {
	       		if(hostname==null) { 
	       		  System.out.println("Syntax: clienteUDP <hostname> <port>"); return;
	       		}
	       		if(hostname.isEmpty() || port==0) {
	       			System.out.println("Syntax: clienteUDP <hostname> <port>"); return;			
	       		}
	       		
	        	System.out.println("Starting UDP Cliente......");
	        	this.hostname = hostname;
				this.port = port;
				this.msg=m;
				this.timer=timer;
			
	        
	    }
	   
	    public void run() {
	    	  running = true;
	    	
	    	 try {
	    		
	              address = InetAddress.getByName(hostname);
	              socket = new DatagramSocket();
	              App.LOGGER.info("[clienteUDP] Connected with "+hostname+" Running...");
	              System.out.println("[clienteUDP] Connected with "+hostname+" Running...");
	            
	             while (running) {
	            	// String name = Thread.currentThread().getName();
	           	 
	                 synchronized (msg) {
	                     try{
	                    	//System.out.println(msg.getMsg());
	                        // System.out.println(" waiting to get notified at time:"+System.currentTimeMillis());
	                         msg.wait();
	                        
	                     }catch(InterruptedException e){
	                         e.printStackTrace();
	                     }
	                     //System.out.println(name+" waiter thread got notified at time:"+System.currentTimeMillis());
	                     //process the message now
	                    System.out.println(address+" processed: "+msg.getMsg());
	                    // System.out.println("Enviar a "+this.hostname+" : "+this.port);
		                 DatagramPacket request = new DatagramPacket(msg.getMsg().getBytes(),msg.getMsg().length(), address, port);
		                 
		                 socket.send(request);	  
	                 }

		            try{
		            	   
		                 Thread.sleep(timer);
		             } catch (InterruptedException e)  {
		                 Thread.currentThread().interrupt(); 
		                // Log.error("Thread interrupted", e); 
		                 App.LOGGER.severe("[clienteUDP] Thread was interrupted, Failed to complete operation");
		             }
	             }
	  
	         } catch (SocketTimeoutException ex) {
	             System.out.println("[clienteUDP] Timeout error: " + ex.getMessage());
	             App.LOGGER.severe("[clienteUDP] Timeout error: "+ ex.getMessage());
	             //ex.printStackTrace();
	         } catch (IOException ex) {
	             System.out.println("[clienteUDP] Client error: " + ex.getMessage());
	             App.LOGGER.severe("[clienteUDP] IOException error:" + ex.getMessage());
	             //ex.printStackTrace();
	         }
	    }

		public boolean isRunning() {
			return running;
		}

		public void setRunning(boolean running) {
			this.running = running;
		}

		public Data getMsg() {
			return msg;
		}

		public void setMsg(Data msg) {
			this.msg = msg;
		}
	    
	    
	 
 
 
       
}
