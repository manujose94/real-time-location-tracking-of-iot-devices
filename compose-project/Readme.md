# Launch project via Docker-Compose


The docker-compose file defines a multi-container application that consists of three services:

- **mqtt-broker**: This service uses the Eclipse Mosquitto MQTT broker image and exposes port 1883 for communication with other services. It also creates volumes for storing data, configuration files, and log files.
- **IoT-devices**: This service builds an IoT device simulator using the context of the "./simulatorIoT" directory and depends on the mqtt-broker service. The IoT-devices service can communicate with the mqtt-broker service to publish or subscribe to MQTT topics and exchange data.
- **comm-manager**: This service builds a communication manager using the context of the "./CommunicationsManager" directory. It exposes port 8025 and uses environment variables from the .env file to configure its settings. It depends on the mqtt-broker service and can use it to exchange data with IoT-devices.
- **visualiser**: This service builds a visualizer using the context of the "./Visualiser" directory. It exposes port 8005 and uses the code from the "/usr/src/app/" directory to render a map showing the location of the IoT-devices. It depends on the mqtt-broker service and can use it to receive data from IoT-devices.

Overall, this project is an IoT-based application that simulates IoT devices, communicates with them using MQTT, and visualizes their location on a map. It uses Docker to containerize the application and manage the dependencies between its different components.

### Launching the Docker Compose Project
1. Navigate to the directory where the docker-compose.yml file is located.

2. Open a terminal window in that directory.

3. Run the following command to start the Docker Compose project:


```
docker-compose up
```


Wait for the services to start up and for the messages to appear in the terminal.

### Restarting the Docker Compose Project
1. Open a terminal window in the directory where the docker-compose.yml file is located.

2. Run the following command to stop the Docker Compose project:

   ```
   docker-compose down
   ```

3. 
   Wait for the services to stop.

4. Run the following command to start the Docker Compose project:


```
docker-compose up
```

​		Wait for the services to start up and for the messages to appear in the terminal.

### Deleting the Docker Compose Project
1. Open a terminal window in the directory where the docker-compose.yml file is located.

2. Run the following command to stop and remove the Docker Compose project:

   ```
   docker-compose down --volumes
   ```

   Wait for the services to stop and for the volumes to be removed.

### Recreating the Docker Compose Project with no-cache
1. Open a terminal window in the directory where the docker-compose.yml file is located.

2. Run the following command to recreate the Docker Compose project with no cache:

   ```
   docker-compose build --no-cache
   ```

   Wait for the services to be rebuilt with no cache.

> Note: Using --no-cache will force Docker to rebuild the images from scratch instead of using any cached layers. This may take longer to complete but can be useful if you want to ensure that you have the latest dependencies and code.