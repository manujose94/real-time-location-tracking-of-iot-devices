#!/bin/bash
#Problems
#Server.py must to have a correctly established IP (Localhost, static Ip from host)
# Future performances: Sending IP parameter to Serve.py
#Global paths for implemented server.
pathProjectProxys=/usr/local/bin/ProjectPozyx
pathVisualiser=/usr/local/bin/ProjectPozyx/visualiserPOZYX
pathServicer=/usr/local/bin/ProjectPozyx/CommunicationsManager
pathSystemServices=/etc/systemd/system/
pathSystemServicesfile1=/etc/systemd/system/CommunicationsManager.service
pathSystemServicesfile2=/etc/systemd/system/Visualiser.service
pathSystemShellfile1=/usr/local/bin/ProjectPozyx/CommunicationsManager/CommunicationsManager.sh
pathServicerJar=$pathServicer/communicationsmanager.jar
pathVisualiserIdenx=$pathVisualiser/index.html
#Files the initial project folder
fileService1=CommunicationsManager.service
fileService2=Visualiser.service
fileForService1=ProjectPozyx/CommunicationsManager/CommunicationsManager.sh
fileJar=ProjectPozyx/CommunicationsManager/communicationsmanager.jar
fileCSS=ProjectPozyx/CommunicationsManager/w3.css
fileVisualiserTAR=ProjectPozyx/visualiserPOZYX/visualiserPOZYX.tar.gz

#Creation methods
createFolder() {
    mkdir $pathProjectProxys
}
createSubfolders(){
    mkdir $pathVisualiser
    mkdir $pathServicer
}

deleteSubfolders(){
    rm -rf $pathVisualiser
    rm -rf $pathServicer
}
deleteFolderProject(){
    rm -rf $pathProjectProxys
}
#SERVICES
SERVICE=CommunicationsManager
SERVICE2=Visualiser
checkServices(){
#For to prove, you migtht use con command.
	STATUS=$(service $SERVICE status | grep -i 'running\|stopped' | awk '{print $3}')
	if [[ ( ! -z "$STATUS" ) && $STATUS = "(running)" ]]; then
		echo "$SERVICE is running!!!"
	else
		if [ -z "$STATUS" ]; then		
		echo "$SERVICE has not yet implemented"
		else
		echo "$SERVICE is stopped!!!"
		fi
	fi
	STATUS2=$(service $SERVICE2 status | grep -i 'running\|stopped' | awk '{print $3}')
	if [[ ( ! -z "$STATUS2" ) && $STATUS2 = "(running)" ]]; then
		echo "$SERVICE2 is running!!!"
	else
		if [ -z "$STATUS2" ]; then		
		echo "$SERVICE2 has not yet implemented"
		else
		echo "$SERVICE2 is stopped!!!"
		fi
	fi
}
# NOTE
# It's important that before to launch specific services, it's needed all code is implemented inside its corresponding folder
#
launchServices(){
	if [ ! -f $pathSystemShellfile1 ]; then
	      echo "File $pathSystemShellfile1 not exist [FAILED] "
	      return
	fi
	if [ ! -f $pathSystemServicesfile1 ]; then
	      echo "File $pathSystemServicesfile1 not exist [FAILED] "
	      return
	fi
	if [ ! -f $pathSystemServicesfile2 ]; then
	      echo "File $pathSystemServicesfile2 not exist [FAILED] "
	      return
	fi
	
	STATUS=$(service $SERVICE status | grep -i 'running\|stopped' | awk '{print $3}')
	if [[ ( ! -z "$STATUS" ) && $STATUS = "(running)" ]]; then
		echo "$SERVICE is running, then restar service!!!"
		systemctl start $SERVICE2
	else
		if [ -z "$STATUS" ]; then		
			echo "$SERVICE has not yet implemented"
			cp $fileService1 $pathSystemServices
			systemctl daemon-reload
		    systemctl enable $SERVICE
			systemctl start  $SERVICE
		else
			echo "$SERVICE is stopped!!!"
   			systemctl start $SERVICE
		fi
	fi
	STATUS2=$(service $SERVICE2 status | grep -i 'running\|stopped' | awk '{print $3}')
	if [[ ( ! -z "$STATUS2" ) && $STATUS2 = "(running)" ]]; then
		echo "$SERVICE2 is running, then restart service!!!"
		systemctl start $SERVICE2
	else
		if [ -z "$STATUS2" ]; then	
			echo "$SERVICE2 has not yet implemented"
			cp $fileService1 $pathSystemServices
			systemctl daemon-reload
		    	systemctl enable $SERVICE2
			systemctl start  $SERVICE2
		else
			echo "$SERVICE2 is stopped!!!"
			systemctl start $SERVICE2
		fi
	fi
}
deleteServices(){
	if [ -f "$pathSystemServicesfile1" ]; then #Service 1 exist
		systemctl stop $SERVICE1
		systemctl disable $SERVICE1
		rm $pathSystemServicesfile1
		echo "$pathSystemServicesfile1 removed"
	else 
		echo "$fileService1 does not exist"
	fi
	if [ -f "$pathSystemServicesfile2" ]; then #Service 2 exist
		systemctl stop $SERVICE2
		systemctl disable $SERVICE2
		rm $pathSystemServicesfile2
		echo "$pathSystemServicesfile2 removed"
	else 
		echo "$fileService2 does not exist"
	fi
	systemctl daemon-reload
	systemctl reset-failed
}
startCheckingStructure(){
if [ ! -d $pathProjectProxys ]; then
	      echo "[D] $pathProjectProxys [NOT FOUND]"
else 
		  echo "[D] $pathProjectProxys [OK]"
fi
if [ ! -d $pathVisualiser ]; then
	      echo "[D] $pathVisualiser [NOT FOUND WEB]"
else 
 echo "[D] $pathVisualiser [OK]"
fi
if [ ! -d $pathServicer ]; then
	      echo "[D] $pathServicer [NOT FOUND]"
else 
echo "[D] $pathServicer [OK]"		  
fi
if [ ! -f $pathServicerJar ]; then
		echo "[F] $pathServicerJar [JAR NOT FOUND] "
else 
echo "[F] $pathServicerJar [OK] "
fi
if [ ! -f $pathSystemShellfile1 ]; then
		echo "[F] $pathSystemShellfile1 [SHELL LAUNCH NOT FOUND] "
else
echo "[F] $pathSystemShellfile1 [OK] "
fi
if [ ! -f $pathSystemServicesfile1 ]; then
		echo "[F] $pathSystemServicesfile1 [SERVICE NOT FOUND] "
else 
echo "[F] $pathSystemServicesfile1 [OK] "
fi
if [ ! -f $pathSystemServicesfile2 ]; then
		echo "[F] $pathSystemServicesfile2 [SERVICE NOT FOUND] "
else 
echo "[F] $pathSystemServicesfile1 [OK] "
fi

}
CheckingStructure(){
# if all okey, start services
# sudo systemctl status CommunicationsManager 

if [[ -d "$pathProjectProxys" && -d "$pathServicer" && -d "$pathVisualiser" ]];then
	echo "Files already there exist"
	echo "All project is established in this root folder: "$pathProjectProxys
	echo "Inside contain these folders: $pathServicer , pathVisualiser" 
	echo -n "You want to delete all and restart (y/n)? "
	read answer2
	if [ "$answer2" != "${answer2#[Yy]}" ] ;then #Delete all project and create structure folder(1)
		deleteFolderProject
		createAllProject
		 return 2
	else
	     echo "Exitting...."
	     return 0
	     exit
	fi

else
	echo "Currently, there isn't project ($pathProjectProxys) implemented"
	createAllProject
fi
}

createAllProject(){
	createFolder
	createSubfolders
	if [[ -d "$pathServicer" && -d "$pathVisualiser" ]];then #Checking Pozyxs Services and Visualiser (2)
		cp $fileJar $pathServicer
		cp $fileCSS $pathServicer
		tar -xzvf $fileVisualiserTAR -C $pathProjectProxys
		if [ -f "$pathSystemServicesfile1" ]; then #Checkig Services file and copy it (3)
		    echo "$fileService1 exist, if you want to launch other service version (4 Delete all in Menu)"
		else 
		    echo "$fileService1 not exist, then (copy) it from project folder.."
		    cp $fileService1 $pathSystemServices
		fi
		if [ -f "$pathSystemShellfile1" ]; then
		   echo "File (SH) $pathSystemShellfile1 already exist (4 Delete all in Menu)"
		else 
 		    cp $fileForService1 $pathServicer
		fi
		if [ -f "$pathSystemServicesfile2" ]; then
		    echo "$fileService2 exist , if you want to launch other service version (4 Delete all in Menu)"
		else 
		    echo "$fileService12 not exist, then (copy) it from project folder.."
		    cp $fileService2 $pathSystemServices
		fi
		systemctl daemon-reload # changed on disk. Run 'systemctl daemon-reload' to reload units.
		launchServices #Launch Services (4)
		return 2
	fi
}
# 1. Create ProgressBar function
# 1.1 Input is currentState($1) and totalState($2)
function ProgressBar {
	# Process data
		let _progress=(${1}*100/${2}*100)/100
		let _done=(${_progress}*4)/10
		let _left=40-$_done
	# Build progressbar string lengths
		_fill=$(printf "%${_done}s")
		_empty=$(printf "%${_left}s")

	# 1.2 Build progressbar strings and print the ProgressBar line
	# 1.2.1 Output example:                           
	# 1.2.1.1 Progress : [########################################] 100%
	printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"

}

# Variables
_start=1

# This accounts as the "totalState" variable for the ProgressBar function
_end=100
# Progress
progress(){
for number in $(seq ${_start} ${_end})
do
    sleep 0.1
    ProgressBar ${number} ${_end}
done
printf '\nFinished!\n'
}
# Show menu
menu() {
echo ""
echo ""
echo "___________-MENU START-___________"
echo ""
PS3='Please enter main option: '
options=("Initialize project and checking its structure" "Checking current project"
"Checking current services of project" "Create or Restart current project services" "Delete All" "Main menu quit")
select opt in "${options[@]}"
do
    case $opt in
        "Initialize project and checking its structure")
            read -p "Starting to implement the project and services (y/n)? " answer
		case ${answer:0:1} in
		    y|Y )
			echo Yes
			CheckingStructure
			CHECKINGSTRUCTURE_RETURN_CODE=$?
			if [ "$CHECKINGSTRUCTURE_RETURN_CODE" -eq "2" ]; then
				progress
			fi
			menu
		    ;;
		    * )
			menu
		    ;;
		esac
			    ;;
	    "Checking current project")
            startCheckingStructure
	    menu
            ;;
        "Checking current services of project")
            checkServices
	    menu
            ;;
	"Create or Restart current project services")
            launchServices
	    menu
            ;;
	"Delete All")
		read -p "Remove all structure project (y/n)? " answer
		case ${answer:0:1} in
    		y|Y )
        		deleteFolderProject
			echo "Deleted the structure project $pathProjectProxys [OK]"
			deleteServices
			echo "Stopped and Deleted services into /etc/systemd/system/ [OK]"
			menu
		;;
		* )
        		echo "do nothing...."
			menu
		;;
		esac
            ;;
        "Main menu quit")
            exit
            ;;
        *) echo "invalid option $REPLY";;
    esac
done	
}

# main menu
echo " ___________What would you want to do with this project___________"
menu



